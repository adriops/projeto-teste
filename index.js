var request = require('request');

const express = require('express');
const app = express();

// serve files from the public directory
app.use(express.static('public'));

request('https://jsonplaceholder.typicode.com/users', function (error, res, body) {	
  global.jsonData = JSON.parse(body); 
  console.log(jsonData); 
});
  
// start the express web server listening on 8080
app.listen(3000, () => {
  console.log('listening on 3000');
});

// serve the homepage
app.get('/', (req, res) => {
  res.send(JSON.stringify(jsonData));
  res.sendFile(__dirname + '/index.html');
});

//end point para lista de websites
app.get('/listWebsites', function(req,res){
	var dados="<html><body>";
	 for(var i = 0; i < jsonData.length; i++) {
	 	dados= dados+"name: "+jsonData[i].name+"<br>"
	 				+"website:"+jsonData[i].website+"<br><br>";                   
     }
     dados= dados+"</body></html>";
    res.send(dados);
});

//end point para lista em ordem alfabética
app.get('/listOrderByName', function(req,res){
	jsonData.sort(function(a, b) {
    return (a.name > b.name) - (a.name < b.name)//parseFloat(a.name) - parseFloat(b.name);
});
	var dados="<html><body>";
	 for(var i = 0; i < jsonData.length; i++) {
	 	dados= dados+"name: "+jsonData[i].name+"<br>"
	 				+"email: "+jsonData[i].email+ "<br>"
	 				+"Company: "+jsonData[i].company.name+"<br><br>";                   
     }
     dados= dados+"</body></html>";
    res.send(dados);
});

//end point para lista de usuários com 'suite' no endereço
app.get('/listWithSuite', function(req,res){	
	var dados="<html><body>";
	 for(var i = 0; i < jsonData.length; i++) {
	 	if (jsonData[i].address.suite.toLowerCase().indexOf("suite") !== -1) {
	 		dados= dados+"name: "+jsonData[i].name+"<br>"
	 				+"address: "+jsonData[i].address.street+ "<br>"
	 				+"suite: "+jsonData[i].address.suite+"<br><br>";
	 	}	 	                   
     }
     dados= dados+"</body></html>";
    res.send(dados);
});







