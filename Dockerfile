FROM node

WORKDIR .

COPY package.json .

RUN npm install
COPY . .
EXPOSE 3000

ENV NAME project
CMD node index.js