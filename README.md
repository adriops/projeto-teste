# Aplicação de teste para Mutant

## Pré-requisitos

- Docker 18.09 ou superior (Linux ou Windows 10 )
- Docker ToolBox para Windows (versões anteriores ao Windows 10)

## Rodando a aplicação Linux ou Windows 10
    
	- No console rodar o comando:
	
	docker run -p 8080:3000 adriops/repository:mutant
	
## Rodando a aplicação versões anteriores do windows

    - Abrir o programa kitematic;
	
	- Com o kitematic aberto, rodar o comando no console:
	
	docker run -p 8080:3000 adriops/repository:mutant

## Executando no navegador

http://localhost:8080 (para Linux ou Windows 10)

http://192.168.99.100:8080 (versões anteriores ao Windows 10)
